using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractDoor : MonoBehaviour
{
    public float distancia = 3f;
    [SerializeField] private AudioClip audiosource;
    


    void FixedUpdate()
    {
        RaycastHit hit;
        Debug.DrawRay(transform.position + Vector3.up * 1.5f, transform.TransformDirection(Vector3.forward));
       // Debug.Log(transform.position + Vector3.up * 1.5f + "          " + transform.TransformDirection(Vector3.forward));
        if (Physics.Raycast(transform.position+Vector3.up*1.5f, transform.TransformDirection(Vector3.forward),out hit, distancia))
        {
            if (hit.collider.tag == "Door")
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    hit.collider.transform.GetComponent<Door>().ChangeDoorState();
                    AudioManager.Instance.EjecutarSonido(audiosource);

                }
            }
        }


    }
}
