using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InfernalDoor : MonoBehaviour
{
    public static bool infernalKey;
    public bool open;
    public bool close;
   
    public bool inTrigger;

    void OnTriggerEnter(Collider other)
    {
        inTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        inTrigger = false;
    }

    void Update()
    {
        if (inTrigger)
        {
            if (close)
            {
                if (infernalKey)
                {
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        open = true;
                        close = false;
                    }
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    close = true;
                    open = false;
                }
            }
        }

        if (open)
        {
           // Debug.Log("Hola");
            SceneManager.LoadScene(3); 
        }
        
    }

    void OnGUI()
    {
        if (inTrigger)
        {
            if (open)
            {
                GUI.Box(new Rect(0, 0, 200, 25), "Presiona la tecla E para cerrar");
            }
            else
            {
                if (infernalKey)
                {
                    GUI.Box(new Rect(0, 0, 400, 25), "Pulsa E para avanzar");
                }
                else
                {
                    GUI.Box(new Rect(0, 0, 400, 25), "Necesitas la llabe dropeada por el Boss para avanzar");
                }
            }
        }
    }
}
