using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool dooropen = false;
    public float doorOpenAngle= 95f;
    public float doorCloseAngle=0f;
    public float smooth = 3f;


    public void ChangeDoorState()
    {
        dooropen = !dooropen;
    }
    


    void Update()
    {
        if (dooropen)
        {
            Quaternion targetRot = Quaternion.Euler(0, doorOpenAngle, 0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRot, smooth * Time.deltaTime);

        }
        else
        {
            Quaternion targetRot2 = Quaternion.Euler(0, doorCloseAngle, 0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRot2, smooth * Time.deltaTime);
        }
    }

   
}
