using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateTramp : MonoBehaviour
{
    private Animator anim;
    private bool activada;
    private bool hasPlayed;
    public AudioSource audiosource;

    private void Start()
    {
        anim = GetComponent<Animator>();
        hasPlayed = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!hasPlayed)
        {
            if (other.tag == "Player")
            {
                Trampa();
                PlaySound();
            }
        }
    }
    private void PlaySound()
    {
        audiosource.Play();
        hasPlayed = true;
    }
    public void Trampa()
    {
        anim.SetBool("Activar", true);

        StartCoroutine(Desactivar());
    }

    IEnumerator Desactivar()
    {
        //anim.SetBool("Activar", true);
        anim.SetBool("Desactivar", false);
        yield return new WaitForSeconds(3f);
        anim.SetBool("Desactivar", true);
        anim.SetBool("Activar", false);
        hasPlayed = false;

    }
}
