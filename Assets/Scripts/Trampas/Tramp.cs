using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tramp : MonoBehaviour
{
    public float damage;
   // [SerializeField] AudioSource audiocut;


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<Player>().TakeDamage(damage);
           // audiocut.Play();
        }
    }

    //Hace dao mientras este tocando el collider
    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.tag == "Player")
    //    {
    //        other.GetComponent<Player>().TakeDamage(damage);

    //    }
    //}

}
