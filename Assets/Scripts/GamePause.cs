using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePause : MonoBehaviour
{
    public GameObject pauseMenu;
    public Scout player;
    public bool isInventory;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (pauseMenu.activeSelf)
        {
            Time.timeScale = 0;
            player.sensitivity = 0f;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else if(!isInventory)
        {
            Time.timeScale = 1;
            player.sensitivity = 2.0f;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            isInventory = !isInventory;
        }
        if (isInventory)
        {
            Time.timeScale = 0;
            player.sensitivity = 0f;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
