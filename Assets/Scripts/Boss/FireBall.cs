using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    private float cronometro;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * 6 * Time.deltaTime);
        transform.localScale += new Vector3(3, 3, 3) * Time.deltaTime;

        cronometro += 1 * Time.deltaTime;

        if (cronometro >1f)
        {
            transform.localScale = new Vector3(1, 1, 1);
            //gameObject.SetActive(false);
            Destroy(this.gameObject);
            cronometro = 0;
        }
    }
}
