using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoBoss : MonoBehaviour
{
    public Animator anim;
    public Boss boss;
    public int melee;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            melee = Random.Range(0, 3);
            switch (melee)
            {
                case 0:
                    anim.SetFloat("Ataques", 0);
                    boss.hit_select = 0;
                    break;

                case 1:
                    anim.SetFloat("Ataques", 0.2f);
                    boss.hit_select = 1;
                    break;

                case 2:
                    anim.SetFloat("Ataques", 0.4f);
                    boss.hit_select = 2;
                    break;
                /*case 3:
                    //FireBall
                    if (boss.fase==2)
                    {
                        anim.SetFloat("Ataques", 1);
                    }
                    else
                    {
                        melee = 0;
                    }
                    break;*/
            }
            anim.SetBool("walk", false);
            anim.SetBool("run", false);
            anim.SetBool("attack", true);
            boss.atacando = true;
            GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}
