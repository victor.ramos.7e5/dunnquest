using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Boss : MonoBehaviour
{
    public NavMeshAgent agent;

    private Transform player;

    private int rutina;
    private float cronometro;
   
    public float time_rutina;
    private Animator anim;
    private Quaternion angulo;
    private float grado;
    public bool atacando;
    public RangoBoss rango;
    public float speed;
    public GameObject[] hit;
    public int hit_select;


    //Lanzallamitas de don charizaldo//
    public bool lanzallamas;
    public List<GameObject> pool = new List<GameObject>();
    public GameObject fire;
    public GameObject cabeza;
    private float cronometro2;
    //Ataque de Salto//
    private float jump_distance;
    private bool direction_Skill;

    //Fire Ball//
    //public GameObject fireball;
    public GameObject point;
    //private List<GameObject> pool2 = new List<GameObject>();


    ////////////
    public int fase = 1;
    public float HP_Min;
    public float HP_Max;
    public Image barra;
    public AudioSource musicaboss;
    public AudioSource musicajuego;
    private bool muelto;


    [SerializeField] GameObject itemsSpawn;
    private void Start()
    {
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();

    }
   


    void Update()
    {
        barra.fillAmount = HP_Min / HP_Max;
        if (HP_Min > 0)
        {
            Vivo();
        }
        else
        {
            if (!muelto)
            {
                anim.SetTrigger("dead");
                musicaboss.enabled = false;
                musicajuego.enabled = true;
                muelto = true;
                SpawnWinCond();
            }
            
        }
    }
    public void Vivo()
    {
        if (HP_Min < 500)
        {
            fase = 2;
            time_rutina = 1;
        }
        Comportamiento_boss();
        if (lanzallamas)
        {
            Lanzallamas_Skill();
        }
    }

    public void Comportamiento_boss()
    {
        if (Vector3.Distance(transform.position, player.transform.position) < 15)
        {
            var lookpose = player.transform.position - transform.position;
            lookpose.y = 0;
            var rotation = Quaternion.LookRotation(lookpose);

            point.transform.LookAt(player.transform.position);

            barra.gameObject.SetActive(true);
            musicajuego.enabled = false;
            musicaboss.enabled = true;



            if (Vector3.Distance(transform.position, player.transform.position) > 1 && !atacando)
            {

                switch (rutina)
                {
                    case 0:
                        //Walk//
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 2);
                        //agent.SetDestination(player.position);

                        anim.SetBool("walk", true);
                        anim.SetBool("run", false);
                        if (transform.rotation==rotation)
                        {
                            transform.Translate(Vector3.forward * speed * Time.deltaTime);
                        }
                        anim.SetBool("attack", false);
                        ////////////
                        cronometro+=1 * Time.deltaTime;
                        if (cronometro>time_rutina)
                        {
                            rutina = Random.Range(0, 4);
                            cronometro = 0;
                        }
                        break;
                    case 1:
                        /////Run
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 2);
                        anim.SetBool("walk", false);
                        anim.SetBool("run", true);
                        if (transform.rotation == rotation)
                        {
                            transform.Translate(Vector3.forward * speed*2 * Time.deltaTime);
                        }
                        anim.SetBool("attack", false);
                        break;
                    case 2:
                        //Lanzallamas
                        anim.SetBool("walk", false);
                        anim.SetBool("run", false);
                        anim.SetBool("attack", true);
                        anim.SetFloat("Ataques", 0.8f);
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 2);
                        rango.GetComponent<CapsuleCollider>().enabled = false;
                        break;
                    case 3:
                        //Jump attack
                        if (fase==2)
                        {
                            jump_distance += 1 * Time.deltaTime;
                            anim.SetBool("walk", false);
                            anim.SetBool("run", false);
                            anim.SetBool("attack", true);
                            anim.SetFloat("Ataques", 0.6f);
                            hit_select = 3;
                            rango.GetComponent<CapsuleCollider>().enabled = false;

                            if (direction_Skill)
                            {
                                if (jump_distance<1f)
                                {
                                    transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 2);
                                }
                                transform.Translate(Vector3.forward * 8 * Time.deltaTime);
                            }
                            else
                            {
                                rutina = 0;
                                cronometro = 0;
                            }
                        }

                        break;
                    /*case 4:
                        //FireBalls
                        if (fase == 2)
                        {
                            jump_distance += 1 * Time.deltaTime;
                            anim.SetBool("walk", false);
                            anim.SetBool("run", false);
                            anim.SetBool("attack", true);
                            anim.SetFloat("Ataques", 1);
                            hit_select = 3;
                            rango.GetComponent<CapsuleCollider>().enabled = false;
                            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 0.5f);
                        }
                        else
                        {
                            rutina = 0;
                            cronometro = 0;
                        }
                            break;
                    default:
                        break;*/
                }
            }
        }
    }

    public void FinalAnim()
    {
        rutina = 0;
        anim.SetBool("attack", false);
        atacando = false;
        rango.GetComponent<CapsuleCollider>().enabled = true;
        lanzallamas = false;
        jump_distance = 0;
        direction_Skill = false;
    }
    public void Direction_Attack_Start()
    {
        direction_Skill = true;
    }
    public void Direction_Attack_Final()
    {
        direction_Skill = false;
    }
    ///Melee
    public void CollliderWeaponTrue()
    {
        hit[hit_select].GetComponent<SphereCollider>().enabled = true;
    } 
    public void CollliderWeaponFalse()
    {
        hit[hit_select].GetComponent<SphereCollider>().enabled = false;
    }

    //Lanzallamas

    public GameObject GetBala()
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (!pool[i].activeInHierarchy)
            {
                pool[i].SetActive(true);
                return pool[i];
            }
        }
        GameObject obj = Instantiate(fire, cabeza.transform.position, cabeza.transform.rotation) as GameObject;
        pool.Add(obj);
        return obj;
    }
    public void Lanzallamas_Skill()
    {
        cronometro += 1 * Time.deltaTime;
        if (cronometro>0.1f)
        {
            GameObject obj = GetBala();
            obj.transform.position = cabeza.transform.position;
            obj.transform.rotation = cabeza.transform.rotation;
            cronometro = 0;
        }
    }
    public void StartFire()
    {
        lanzallamas = true;
    }
    public void StopFire()
    {
        lanzallamas = false;
    }
    private void SpawnWinCond()
    {

         Instantiate(itemsSpawn, this.transform.position, Quaternion.identity);

        Destroy(this.gameObject,15f);
    }
    ///FireBalls

    /*public GameObject GetFireBall()
    {
        for (int i = 0; i < pool2.Count; i++)
        {
            if (!pool2[i].activeInHierarchy)
            {
                pool2[i].SetActive(true);
                return pool2[i];
            }
        }
        GameObject obj = Instantiate(fireball, point.transform.position, point.transform.rotation) as GameObject;
        pool2.Add(obj);
        return obj;
    }*/
    /* public void FireBallSkill()
     {
         GameObject obj = GetFireBall();
         obj.transform.position = point.transform.position;
         obj.transform.rotation = point.transform.rotation;
     }*/

}
