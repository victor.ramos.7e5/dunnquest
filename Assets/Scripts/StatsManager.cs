using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Inventories;

public class StatsManager : MonoBehaviour
{
    public TMP_Text armortxt;
    public TMP_Text dmgtxt;
    public Equipment equipment;
    public float armor;
    public float damage;

    void Start()
    {
        
        armortxt.text = "0";
        dmgtxt.text = "30";
    }

    
    void Update()
    {
        armor = ((equipment.GetItemInSlot(EquipLocation.Body) != null) ? equipment.GetItemInSlot(EquipLocation.Body).armor : 0)
            + ((equipment.GetItemInSlot(EquipLocation.Helmet) != null) ? equipment.GetItemInSlot(EquipLocation.Helmet).armor : 0)
            + ((equipment.GetItemInSlot(EquipLocation.Boots) != null) ? equipment.GetItemInSlot(EquipLocation.Boots).armor : 0)
            + ((equipment.GetItemInSlot(EquipLocation.Necklace) != null) ? equipment.GetItemInSlot(EquipLocation.Necklace).armor : 0)
            + ((equipment.GetItemInSlot(EquipLocation.Shield) != null) ? equipment.GetItemInSlot(EquipLocation.Shield).armor : 0)
            + ((equipment.GetItemInSlot(EquipLocation.Trousers) != null) ? equipment.GetItemInSlot(EquipLocation.Trousers).armor : 0)
            + ((equipment.GetItemInSlot(EquipLocation.Gloves) != null) ? equipment.GetItemInSlot(EquipLocation.Gloves).armor : 0);
        armortxt.text = "" + armor;
        damage = 30f + ((equipment.GetItemInSlot(EquipLocation.Weapon) != null) ? equipment.GetItemInSlot(EquipLocation.Weapon).damage : 0)
            + ((equipment.GetItemInSlot(EquipLocation.Shield) != null) ? equipment.GetItemInSlot(EquipLocation.Shield).damage : 0)
            + ((equipment.GetItemInSlot(EquipLocation.Necklace) != null) ? equipment.GetItemInSlot(EquipLocation.Necklace).damage : 0);
        dmgtxt.text = "" + damage;
    }
}
