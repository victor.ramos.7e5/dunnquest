using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootMulti : MonoBehaviour
{
    public float shootForce;
    public Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = rb.transform.forward * shootForce;
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
