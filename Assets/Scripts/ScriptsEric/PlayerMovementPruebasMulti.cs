using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerMovementPruebasMulti : NetworkBehaviour
{

    public float movementSpeed = 7f;
    public float rotationSpeed = 500f;
    public float positionRange = 5f;
    public Animator animator;

    // Start is called before the first frame update
    
    void Start()
    {
    }

    public override void OnNetworkSpawn()
    {
        transform.position = new Vector3(Random.RandomRange(positionRange, -positionRange), 0, Random.RandomRange(positionRange, -positionRange));
        transform.rotation = new Quaternion(0, 100, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsOwner) return;
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 movementDirection = new Vector3(horizontalInput, 0, verticalInput);
        movementDirection.Normalize();

        transform.Translate(movementDirection * movementSpeed * Time.deltaTime, Space.World);

        if (movementDirection != Vector3.zero)
        {
            animator.SetBool("IsWalking", true);
            Quaternion toRotation = Quaternion.LookRotation(movementDirection, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }
        else {
            animator.SetBool("IsWalking", false) ;
        };

    }
}
