using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterDATA", menuName = "CharacterSO")]

public class CharacterSO : ScriptableObject
{
    public float speed;
    public float sprintSpeed;
    public float jumpSpeed;
    public float armor;
    public float health;
    public float mana = 100;
    public float damageMelee;
    public float damageSpecial;
    public float damageSpecial2;
    public float damageSpecial3;
    public float crouchHeight = 0.5f;
    public float normalHeight = 2.0f;
}
