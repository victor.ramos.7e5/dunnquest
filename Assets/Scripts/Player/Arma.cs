using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : MonoBehaviour
{
    public float damage;
    //EnemyController _enemy;
    public StatsManager damageAdd;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boss"))
        {
            other.GetComponent<Boss>().HP_Min -= damage;
        }
       /* if (other.CompareTag("Enemy"))
        {
            _enemy.TakeDamage(damage);
        }*/
    }

    public void Update()
    {
        damage = damageAdd.damage;
    }
}
