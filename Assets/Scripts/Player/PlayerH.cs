using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerH : MonoBehaviour
{

    //Player variables
    private float speed = 6.0f;
    private float sprintSpeed = 12.0f;
    private float jumpSpeed = 8.0f;
    private float gravity = 9.8f;
    private float health = 100;
    private float mana = 100;
    public float damageMelee;
    public float damageSpecial;

    public float crouchHeight = 0.5f;
    public float normalHeight = 2.0f;
    private bool isCrouching = false;
    private bool InventoryOpened;
    private float hInput;
    private float vInput;

    //HUD
    public Slider hp_slider;
    public Slider mana_slider;

    //Camera variables
    public Camera cam;
    private float rotationX = 0.0f;
    private float rotationY = 0.0f;
    public float sensitivity = 2.0f;


    //Player components
    private CharacterController characterController;
    [SerializeField] private Canvas inventory;
    private Animator anim;

    private Vector3 move = Vector3.zero;

    Enemies _enemigo;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        characterController = GetComponent<CharacterController>();
        InventoryOpened = false;
        inventory.enabled = false;

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        hInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");
        var vector = new Vector2(hInput, vInput).normalized;
        Debug.Log(anim);
        Debug.Log(anim.GetFloat(1));
        anim.SetFloat("xAxis", vector.x);
        anim.SetFloat("yAxis", vector.y);
        //if (InventoryOpened == false)
        //{

        Move();
        Crouch();
        Jump();
        Camera();

        if (Input.GetKeyDown(KeyCode.Tab) && InventoryOpened == false)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            inventory.enabled = true;
            InventoryOpened = true;
        }
        else if (Input.GetKeyDown(KeyCode.Tab) && InventoryOpened == true)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            inventory.enabled = false;
            InventoryOpened = false;
        }
        hp_slider.value = health;
        mana_slider.value = mana;

    }

    public virtual void Move()
    {
        hInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");
        if (characterController.isGrounded)
        {
            move = new Vector3(hInput, 0.0f, vInput);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                move = transform.TransformDirection(move) * sprintSpeed;
            }


            move = transform.TransformDirection(move) * speed;

            if (Input.GetKey(KeyCode.Space))
            {
                move.y = jumpSpeed;
            }


        }

        move.y -= gravity * Time.deltaTime;

        characterController.Move(move * Time.deltaTime);
    }

    void Crouch()
    {
        // Crouch if left control is pressed
        if (Input.GetKey(KeyCode.LeftControl))
        {
            isCrouching = true;
            characterController.height = crouchHeight;

            //an.SetBool("Crouch", true);
        }
        else
        {
            isCrouching = false;
            characterController.height = normalHeight;

            //an.SetBool("Crouch", false);
        }

    }

    public virtual void Jump()
    {
        if (characterController.isGrounded)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                move.y = jumpSpeed;
            }

        }
    }



    public virtual void Camera()
    {
        rotationX += Input.GetAxis("Mouse X") * sensitivity;
        rotationY += Input.GetAxis("Mouse Y") * sensitivity;
        rotationY = Mathf.Clamp(rotationY, -90.0f, 90.0f);
        transform.localRotation = Quaternion.Euler(0.0f, rotationX, 0.0f);
        cam.transform.localRotation = Quaternion.Euler(-rotationY, 0.0f, 0.0f);

    }



    public virtual void TakeDamage(int damage)
    {
        health -= damage;
        Debug.Log("Health: " + health);
    }


    private void OnTriggerEnter(Collider other)
    {
        var character = other.GetComponent<Player>();
        if (character != null)
        {
            var attackable = GetComponent<IAttackable>();
            if (attackable != null)
            {
                attackable.Attack(character);
            }
        }
    }
}
