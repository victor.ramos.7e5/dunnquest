using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewStriker : Player
{
    public enum ModosStriker
    {
        Normal, // el modo normal como el nombre dice ser� sin los pu�os amarillos solo usar� los pu�os y patadas
        DPS // el modo DPS es activar los pu�os amarillos, en este modo solo pegar� pu�os y no patadas
    }

    public ModosStriker modo;
    public float rawDamage;


    void LateUpdate()
    {

        CambiarModo();


    }
    public void CambiarModo()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (modo == ModosStriker.DPS)
            {
                modo = ModosStriker.Normal;
            }
            else
            {
                modo = ModosStriker.DPS;
            }
        }

    }

    public void FuncionesDeModos()
    {
        switch (modo)
        {
            case ModosStriker.DPS:
                DamageMode();
                break;
            case ModosStriker.Normal:
                NormalMode();
                break;
        }
    }

    private void NormalMode()
    {

    }

    private void DamageMode()
    {

    }


}
