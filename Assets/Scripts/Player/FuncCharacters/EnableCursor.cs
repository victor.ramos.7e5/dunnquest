using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableCursor : MonoBehaviour
{
    private Camera cam;
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        //Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
      
    }
}
