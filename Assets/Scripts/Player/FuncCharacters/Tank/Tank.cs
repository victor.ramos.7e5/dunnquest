using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : Player
{



    //Player components
    private Animator an;
    private CharacterController cc;
    public enum ModosTanque
    {
        Tank, // en este modo ser� como en overwatch la habilidad de tanqueo de doomfist, no podr� pegar y solo tanquear�
        DPS // pega como padrastro borracho
    }
    
    //Tank variables
    public ModosTanque modo;
    public float tankModeSpeed = 3f;
    public float normalSpeed = 6f;
    public float rawDamage;

    //Ability1
    [SerializeField] private GameObject rockhand;
    [SerializeField] private GameObject rocktothrow;
    public float rockvelocity = 10f;

    private bool can_throw = true;
    private float CoolDownAbility1 = 1f;
    private Transform rocktransform;
    //Ability2
    private bool can_tackle = true;
    private bool istackling = false;
    private float CoolDownAbility2 = 2f;
    public float dashDistance = 5f;
    public float dashTime = 2f; // El tiempo que dura el dash
    public float dashCooldown = 15f; // El tiempo de espera entre cada dash
    private float dashTimer; // El temporizador del dash
    private bool isDashing = false;
    private Vector3 dashDirection;
    private Coroutine[] corroutines = new Coroutine[4];
    //Ability3

    private bool can_taunt;
    private float CoolDownAbility3 = 2f;




    void Start()
    {
        rockhand.SetActive(false);
        an = GetComponent<Animator>();
        cc = GetComponent<CharacterController>();
    }

    void LateUpdate()
    {

        Ability1();
        Ability2();
        CambiarModo();


    }




    void Ability1()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && can_throw && mana >= 20 && corroutines[0] == null)
        {

            mana -= 20;
         
            an.SetBool("Throw", true);
            corroutines[0] = StartCoroutine(Ability(CoolDownAbility1, 1));
        }
    }

    void Ability2()
    {
        if (corroutines[1] == null && can_tackle && Input.GetKeyDown(KeyCode.Alpha2) && mana >= 8)
        {
            // El personaje realiza un dash
            isDashing = true;
            dashTimer = dashTime;
            dashDirection = transform.forward;
            Debug.Log("vorteleta");
            corroutines[1] = StartCoroutine(Dash());
        }

        // Si el personaje est� realizando un dash, se mueve en la direcci�n indicada
        if (isDashing)
        {
            an.SetBool("Tackle", true);
            cc.Move(dashDirection * dashDistance / dashTime * Time.deltaTime);
            dashTimer -= Time.deltaTime;

            if (dashTimer <= 0f)
            {
                // El dash ha terminado
                isDashing = false;
                an.SetBool("Tackle", false);

            }
        }
    }

    public void endthrow()
    {
        an.SetBool("Throw", false);
    }

    public void showrock()
    {
        rockhand.SetActive(true);
    }
    public void hideandthrowrock()
    {
        rocktransform = rockhand.transform;
        rockhand.SetActive(false);
        Ray throwrock = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit ;
        
        Vector3 inici = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
        Vector3 final = new Vector3(Input.mousePosition.x, Input.mousePosition.y, float.NaN);
        Vector3 direccion = Input.mousePosition - cam.WorldToScreenPoint(rockhand.transform.position);
        GameObject proyectil = Instantiate(rocktothrow, rocktransform.position, Quaternion.identity);
        Rigidbody rbProyectil = proyectil.GetComponent<Rigidbody>();
        rbProyectil.velocity = direccion.normalized * rockvelocity;

    }

    IEnumerator Dash()
    {
        yield return new WaitUntil(() => isDashing == false);
        corroutines[1] = StartCoroutine(Ability(CoolDownAbility2, 2));
    }




    IEnumerator Ability(float cooldown, int i)
    {
        yield return new WaitForSeconds(cooldown);
        corroutines[i - 1] = null;
    }









    public void CambiarModo()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (modo == ModosTanque.DPS)
            {
                modo = ModosTanque.Tank;
            }
            else
            {
                modo = ModosTanque.DPS;
            }
        }

    }

    public void FuncionesDeModos()
    {
        switch (modo)
        {
            case ModosTanque.DPS:
                DamageMode();
                break;
            case ModosTanque.Tank:
                TankMode();
                break;
        }
    }


    public void DamageMode()
    {
        speed = normalSpeed;
    }
    

    public void TankMode()
    {
        speed = tankModeSpeed;
    }
}
