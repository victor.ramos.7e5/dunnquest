using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using Inventories;
using TMPro;

public class Scout : Player
{

    public enum EstadosScout
    {
        Melee,
        Range
    }

    //SLIDERS
    public Slider skill1;
    public Slider skill2;
    public Slider skill3;
    public Slider skill4;

    //Player components
    
    [SerializeField] private GameObject hunt_trap;
    private Animator an;
    private CharacterController cc;
    private int hit_num = 0;
    public EstadosScout estado;
    private int numberOfAttacks = 6;
    private float attackCooldown = 1.25f;
    private bool canAttack = true;
    private bool aiming = false;
    public bool isBlocking;
    private int currentAttack = 0;
    [SerializeField] GameObject spear;
    [SerializeField] GameObject blowgun;
    [SerializeField] GameObject ammo;
    public float power = 10f;
    //Camera 
    public float originalFOV = 60f;
    public float zoomedFOV = 30f;


    //Ability1
    private bool can_trap = true; 
    public float CoolDownAbility1 = 1f;
    //Ability2
    private bool can_roll = true;
    private bool isrolling = false;
    public float CoolDownAbility2 = 6f;
    public float dashDistance = 2.5f;
    public float dashTime = 0.65f; // El tiempo que dura el dash
    public float dashCooldown = 2f; // El tiempo de espera entre cada dash
    private float dashTimer; // El temporizador del dash
    private bool isDashing = false;
    private Vector3 dashDirection;
    private Coroutine[] corroutines = new Coroutine[4];

    //Ability3
    public float CoolDownAbility3 = 2f;
    public float boostTime = 6f;
    private bool canBoostattack = true;
    private float attackboost = 0.5f;
    //Ability4
    public float CoolDownAbility4 = 2f;
    public float combotime = 6f;
    private bool canComboAttack = true;


    // Start is called before the first frame update
    void Start()
    {
        blowgun.SetActive(false);
        an = GetComponent<Animator>();
        cc = GetComponent<CharacterController>();
    }

     void LateUpdate()
    {

        //Aim();
        AttackMelee();
        Consumables();
        //AttackRange();
        Block();
        Ability1();
        Ability2();
        Ability3();
        Ability4();
       // CambiarEstado();
        if(mana<=100) mana += 3.5f * Time.deltaTime;
        mana_slider.value=mana;
        hp_slider.value = health;
        if (health <= 0)
        {
          //  Debug.Log("Muerto");
            anim.SetTrigger("Death");
            Destroy(cc);
            //anim.Play("Die");
            //StartCoroutine(EsperarAnimacion(tiempoEsperaMuerte));
        }

    }

    void Consumables()
    {

        var actionStore = GetComponent<ActionStore>();
        if (Input.GetKeyDown(KeyCode.Z))
        {
            actionStore.Use(0, gameObject);

        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            actionStore.Use(1, gameObject);
        }
    }
    void Aim()
    {

        if (Input.GetMouseButton(1)&&estado==EstadosScout.Range)
        {
           // Debug.Log("pito");
            an.SetBool("Aim", true);
            aiming = true;
            cam.fieldOfView = zoomedFOV;
        }
        else
        {
           // Debug.Log("pito2");
            an.SetBool("Aim", false);
            aiming = false;
            cam.fieldOfView = originalFOV;
        }
    }

    void AttackMelee()
    {
        if (canAttack&& estado == EstadosScout.Melee && Input.GetMouseButtonDown(0))
        {
                currentAttack++;
                if (currentAttack > numberOfAttacks)
                {
                    currentAttack = 1;
                }
                an.SetTrigger("Attack" + currentAttack);
            canAttack = false;
            Invoke("ResetAttackCooldown", attackCooldown);
        };

    }

    //void AttackRange()
    //{
    //    if (aiming&&canAttack && estado == EstadosScout.Range && Input.GetMouseButtonDown(0))
    //    {
    //        Vector3 direccionDisparo = (Input.mousePosition - ammo.transform.position).normalized;
    //        GameObject shot = Instantiate(ammo, blowgun.transform.position, Quaternion.LookRotation(direccionDisparo));
    //        shot.GetComponent<Rigidbody>().AddForce(transform.forward * power, ForceMode.Impulse);
    //        Destroy(shot, 3f);
    //        Invoke("ResetAttackCooldown", attackCooldown);
    //    };
    //
    //}
    void ResetAttackCooldown()
    {
        canAttack = true;
    }

    void Block()
    {
        if ( estado == EstadosScout.Melee && Input.GetMouseButton(1))
        {
            an.SetBool("Block", true);
            isBlocking = true;
        }
        else
        {
            

            an.SetBool("Block", false);
            isBlocking = false;

        }
    }



    public void Ability1()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && can_trap && mana >= 20 && corroutines[0] == null)
        {
            mana -= 20;
            an.SetTrigger("Trap");
            skill1.value = 1f;
        }
    }
    
    public void put_trap()
    {
        var put_trap = Instantiate(hunt_trap, transform.position, Quaternion.identity);
        corroutines[0] = StartCoroutine(Ability(CoolDownAbility1, 1));
    }
    public void Ability2()
    {
        if (!aiming&&can_roll  && Input.GetKeyDown(KeyCode.Alpha2)&&mana >= 15)
        {
            // El personaje realiza un dash
            mana -= 15;
            isDashing = true;
            can_roll = false;
            dashTimer = dashTime;
            dashDirection = transform.forward;
            //Debug.Log("vorteleta");
            corroutines[1] = StartCoroutine(Dash());
            skill2.value = 1f;
        }

        // Si el personaje est� realizando un dash, se mueve en la direcci�n indicada
        if (isDashing)
        {
            an.SetBool("Rolls", true);
            cc.Move(dashDirection * dashDistance / dashTime * Time.deltaTime);
            dashTimer -= Time.deltaTime;

            if (dashTimer <= 0f)
            {
                // El dash ha terminado
                isDashing = false;
                an.SetBool("Rolls", false);
                can_roll = true;
            }
        }
    }

    public void Ability3()
    {
        if (corroutines[2] == null && canBoostattack && Input.GetKeyDown(KeyCode.Alpha3) && mana >= 30)
        {
            mana -= 30;
            // El personaje empieza el boost
            attackCooldown = attackboost;
           // Debug.Log("prueba1 = "+ attackCooldown);
            Invoke("ResetBoost", boostTime);
            skill3.value = 1f;
        }
      
    }

    public void Ability4()
    {
        if (corroutines[3] == null && canComboAttack && Input.GetKeyDown(KeyCode.Alpha4) && mana >= 30&& estado == EstadosScout.Melee  )
        {

            // El personaje empieza el combo
            mana -= 30;
            an.SetTrigger("Combo");
            corroutines[3] = StartCoroutine(Ability(CoolDownAbility4, 4));
            skill4.value = 1f;
        }

    }

    void ResetBoost()
    {
        attackCooldown = 1.25f;
       // Debug.Log("prueba2 = " + attackCooldown);
        corroutines[2] = StartCoroutine(Ability(CoolDownAbility3, 3));

    }


    public void CambiarEstado()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {


            an.SetTrigger("ChangeWeapon");
            if (estado == EstadosScout.Melee)
            {                
                estado = EstadosScout.Range;
                an.SetBool("Range", true);
                an.SetTrigger("ChangeWeapon");
            }
            else
            { 
                estado = EstadosScout.Melee;
                an.SetBool("Range", false);
               // an.SetTrigger("ChangeWeapon");
               
            }
        }

    }

    public void ChangeWeapon()
    {

        if(estado == EstadosScout.Melee)
        {
             blowgun.SetActive(false);
            spear.SetActive(true);
          
        }else if (estado == EstadosScout.Range)
        {
            spear.SetActive(false);
            blowgun.SetActive(true);
        }
    }
  

    IEnumerator Dash()
    {
        yield return new WaitUntil(() => isDashing == false);
        corroutines[1] = StartCoroutine(Ability(CoolDownAbility2, 1));
    }


    IEnumerator Ability(float cooldown, int i)
    {
        yield return new WaitForSeconds(cooldown);

        corroutines[i-1] = null;
    }

    public void AddHP()
    {
        if (health + 50f > 100)
        {
            health = 100;
        }
        else
        {
            health += 50;
        }
    }


    public void AddMana()
    {
        if (mana + 40 > 100)
        {
            mana = 100;
        }
        else
        {
            mana += 40;
        }

    }


    //public void AddArmor(int armorvalue)
    //{
    //    armor+=armorvalue;
    //}


    //public void QuitArmor(int armorvalue)
    //{
    //    armor -= armorvalue;

    //}

    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");

    }

}
