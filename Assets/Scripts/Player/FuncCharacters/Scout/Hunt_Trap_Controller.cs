using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hunt_Trap_Controller : MonoBehaviour
{


    private float DestroyCoolDown = 2f;
    private Animator animator;
    [SerializeField] private AudioClip audiosource;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Debug.Log("col");
            collision.gameObject.GetComponent<ICatcheable>().trapped();
            Trap_Enemy();

        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<ICatcheable>().trapped();
            Trap_Enemy();
            
        }
    }

    void Trap_Enemy()
    {
        animator.SetBool("Trap", true);
        AudioManager.Instance.EjecutarSonido(audiosource);
        StartCoroutine(DestroyTrap());
    }


    IEnumerator DestroyTrap()
    {
        yield return new WaitForSeconds(DestroyCoolDown);
        Destroy(gameObject);
    }

}
