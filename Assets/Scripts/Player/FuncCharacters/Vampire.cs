using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vampire : Player
{
    public enum EstadosVampiro
    {
        Healer,
        DPS
    }

    public EstadosVampiro estado;
    float vida_extra_max=150;
    float hp_extra=0;
    float multiplicador_da�o_vida;

    public GameObject zonadisp;
    public GameObject damageray;
    public GameObject healray;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {

        CambiarEstado();


    }

    public void DamageMode()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            damageray.SetActive(true);
        }
    }

    public void HealerMode()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            healray.SetActive(true);

        }
    }
    public void FuncionesDeEstados()
    {
        switch (estado)
        {
            case EstadosVampiro.DPS:
                DamageMode();
                break;
            case EstadosVampiro.Healer:
                HealerMode();
                break;
        }
    }
    public void CambiarEstado()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (estado==EstadosVampiro.DPS)
            {
                estado = EstadosVampiro.Healer;
            }
            else 
            {
                estado = EstadosVampiro.DPS;
            }
        }

    }

    void Aumentodeda�o()
    {

        if (health > 100)
        {
            hp_extra = health - 100;

        }
        health += hp_extra;
        multiplicador_da�o_vida = 0.015f * hp_extra;
        damageMelee *= multiplicador_da�o_vida;
    }
}
