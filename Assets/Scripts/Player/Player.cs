using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour

{
    public Scout scout;
    public StatsManager armorAdd;
    //Player variables
    protected float speed = 6.0f;
    protected float currentSpeed = 0;
    protected float sprintSpeed = 12.0f;
    protected float jumpSpeed = 8.0f;
    public float gravity = -9.8f;
    public float health;
    public float mana;
    public float armor;
    public float damageMelee;
    public float damageSpecial;
    public float damageSpecial2;
    public float damageSpecial3;

    public float crouchHeight = 0.5f;

    protected bool isCrouching = false;
    protected bool InventoryOpened;
    private float hInput;
    private float vInput;

    //HUD
    public Slider hp_slider;
    public Slider mana_slider;

    //Camera variables
    public Camera cam;
    private float rotationX = 0.0f;
    private float rotationY = 0.0f;
    public float sensitivity = 2.0f;


    //Player components
    public CharacterController characterController;
    [SerializeField] private Canvas hud;

    public Animator anim;

    private Vector3 move;

    Enemies _enemigo;
    [HideInInspector]
    public Vector3 dir;

    //Player SO
    public CharacterSO _characterData;



    private bool invencible = false;
    public float tiempoInvencible=1.2f;
    public float tiempoEspera;
    private float x;


    public void Start()
    {
      
        characterController = GetComponent<CharacterController>();
        InventoryOpened = false;
        hud.enabled = true; ;

        anim = GetComponent<Animator>();


        damageMelee = _characterData.damageMelee;
        speed = _characterData.speed;
        armor = _characterData.armor;
        jumpSpeed = _characterData.jumpSpeed;
        health = _characterData.health;
        mana = _characterData.mana;
        damageSpecial = _characterData.damageSpecial;
        damageSpecial2 = _characterData.damageSpecial2;
        damageSpecial3 = _characterData.damageSpecial3;
        sprintSpeed = _characterData.sprintSpeed;

       
        //GetGolpear();
    }

    public void Update()
    {
        

        hInput = Input.GetAxis("Horizontal");
         vInput = Input.GetAxis("Vertical");
        var vector = new Vector2(hInput, vInput).normalized;
       
        anim.SetFloat("xAxis", vector.x);
        anim.SetFloat("yAxis", vector.y);

        if (vector.x > 0 || vector.y > 0 || vector.x < 0 || vector.y < 0)
        {
            anim.SetBool("isWalking", true);
        }
        else
        {
            anim.SetBool("isWalking", false);
        }
        //if (InventoryOpened == false)
        //{

          //  Crouch();
           // Jump();
            Camera();

        // }


        //move.y += gravity * Time.deltaTime;
        characterController.Move(move * Time.deltaTime);
        transform.Rotate(move * Time.deltaTime);


        //if (Input.GetKeyDown(KeyCode.Tab)&& InventoryOpened==false)
        //{
        //    Cursor.lockState = CursorLockMode.None;
        //    Cursor.visible = true;
        //    hud.enabled = false;
        //    InventoryOpened = true;
        //}
        //else if (Input.GetKeyDown(KeyCode.Tab) && InventoryOpened == true)
        //{
        //    Cursor.lockState = CursorLockMode.Locked;
        //    Cursor.visible = false;
        //    hud.enabled = true;
        //    InventoryOpened = false;
        //}
        hp_slider.value = health;
        mana_slider.value = mana;
        armor = armorAdd.armor;
    }

    private void FixedUpdate()
    {
        GetDirectionMove();
    }
    public float GetDamage()
    {

       // Debug.Log("GetDamage " + damageMelee);
        return damageMelee;
    }


   public void GetDirectionMove()
    {
       

        dir = transform.forward * vInput + transform.right * hInput;

        characterController.Move(dir * speed * Time.deltaTime);
    }

    public void Crouch()
    {
        // Crouch if left control is pressed
        if (Input.GetKey(KeyCode.LeftControl))
        {
            isCrouching = true;
            characterController.height = crouchHeight;

            //an.SetBool("Crouch", true);
        }
        else
        {
            isCrouching = false;
            //characterController.height = normalHeight;

            //an.SetBool("Crouch", false);
        }

    }

    public void Jump()
    {
        if (characterController.isGrounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                anim.SetBool("", true);
                move.y = jumpSpeed;
            }
            //anim.SetBool("", false);
        }

    }


    public void Camera()
    {
        rotationX += Input.GetAxis("Mouse X") * sensitivity;
        rotationY += Input.GetAxis("Mouse Y") * sensitivity;
        rotationY = Mathf.Clamp(rotationY, -90.0f, 90.0f);
        transform.localRotation = Quaternion.Euler(0.0f, rotationX, 0.0f);
        cam.transform.localRotation = Quaternion.Euler(-rotationY, 0.0f, 0.0f);

    }


    public void TakeDamage(float damageamount)
    {
        //Debug.Log("Prueba");
        RestarVida(damageamount - (damageamount / 2f) * (armor / 100f));
        
        //var blocking = scout.isBlocking;
        // Debug.Log(scout.isBlocking + "Blockingerror");
        //if (blocking)
        // {
        //     var dañorecibido = damageamount - ( armor+300);
        //     RestarVida(dañorecibido);

        // }
        // else
        // {
        //     var dañorecibido = damageamount - armor;
        //   RestarVida(dañorecibido);

        // }
    }

    public void DamageReduction()
    {
        
    }
    public void RestarVida(float damagerecive)
    {
        if (!invencible && health > 0)
        {
            health -= damagerecive;
             StartCoroutine(Invulnerabilidad());            // StartCoroutine(EsperarAnimacion(tiempoEspera));
        }

       
    }

    IEnumerator Invulnerabilidad()
    {
        invencible = true;
        yield return new WaitForSeconds(tiempoInvencible);
        invencible = false;
    }


    //IEnumerator EsperarAnimacion(float tiempo)
    //{
    //    var velocidadActual = speed;
    //    speed = 0;
    //    yield return new WaitForSeconds(tiempo);
    //    //GetComponent<MovePlayer>().speed = velocidadActual;
    //    if (health > 0)
    //    {
    //        speed = velocidadActual;
    //    }
    //    else
    //    {

    //       //Debug.Log("Muerto");
    //    }
    //}

    //public void EquipArmor(float armor_value)
    //{

    //    armor += armor_value;
    //}


    //public void UnequipArmor(float armor_value)
    //{

    //    armor -= armor_value;

    //}



}