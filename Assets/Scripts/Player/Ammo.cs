using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public float damage;
    private float tiempoDestruccion =3f;
    EnemyController _enemy;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boss"))
        {
            other.GetComponent<Boss>().HP_Min -= damage;
        }
        if (other.CompareTag("Enemy"))
        {
            _enemy.TakeDamage(damage);
        }
    }
    void Start()
    {
        Destroy(gameObject, tiempoDestruccion);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
