using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemiesdata",menuName ="EnemiesSO")]
public class EnemiesSO : ScriptableObject
{
    public float hp;
    public float damage;
    public float speed;
    public float range;
    public float rangodistanciar;
    public float rangodetenerse;
    public float shootspeed;
    public float cooldown;
}
