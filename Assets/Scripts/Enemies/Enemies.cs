using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class Enemies : MonoBehaviour,ICatcheable
{

    public enum EnemyType
    {
        SpawnerMelee,  // Enemigo Que espaunea y que si ve al jugador va hacia el 
        PatrullaMelee, // Enemigo de Patrulla que si el jugador entra en su rango va hacia el y si sale vuelve a patrullar
        SpawnerArcher,
        PatrullaArcher,
        PlantaTorreta,
        Trapped
    }

    private float speed;
    private float damage;
    private float detectionRange;
    private float hp;
    private float rangodistanciar;
    private float rangodetenerse;
    private float shootspeed = 10f;
    private float timetoShoot = 1.3f;
    private float speedsaver;

    public EnemyType type;
    public EnemiesSO _SOenemy;
    private Transform player;
    private EnemyType currentState;


    public Transform[] waypoints;
    private int currentWaypointIndex = 0;
    private bool isPlayerDetected = false;



    public NavMeshAgent agent;


    public GameObject bala;
    public Transform zonadisparo;
   
    float originalTime;

    Transform enemy; //Mirar jugador



    public void Attack(Player player)
    {

        player.TakeDamage(damage);
    }

    void Start()
    {
        originalTime = timetoShoot;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        hp = _SOenemy.hp;
        detectionRange = _SOenemy.range;
        damage = _SOenemy.damage;
        speed = _SOenemy.speed;
        rangodistanciar = _SOenemy.rangodistanciar;
        rangodetenerse = _SOenemy.rangodetenerse;
        agent = GetComponent<NavMeshAgent>();


        speedsaver = speed;
    }

    // Update is called once per frame
    void Update()
    {
        TypeEnmeie();
      
    }

    public void TypeEnmeie()
    {
        switch (type)
        {
            case EnemyType.SpawnerMelee:
                DetectarJugador();

                break;
            case EnemyType.PatrullaMelee:
                EnemyPatrolFollow();
                break;
            case EnemyType.SpawnerArcher:
                DetectarJugador();
                break;
            case EnemyType.PatrullaArcher:
                EnemyPatrolFollow();

                break;
            case EnemyType.PlantaTorreta:
                DetectarJugador();

                break;
            case EnemyType.Trapped:
                MirarJugador();
                break;
            default:
                break;
        }
    }

    public void EnemyPatrolFollow()
    {
        if (!isPlayerDetected)
        {
            // Si el jugador no ha sido detectado, continuar patrullando entre los waypoints
             PatrolWaypoints();
        }
        else
        {
            ChasePlayer();
        }
    }

    void PatrolWaypoints()
    {
        // Mover al enemigo hacia el waypoint actual
        agent.destination = waypoints[currentWaypointIndex].position;

        if (Vector3.Distance(transform.position, waypoints[currentWaypointIndex].position) < 0.65f)
        {

            currentWaypointIndex = (currentWaypointIndex + 1) % waypoints.Length;
        }

        // Si el jugador est� dentro del rango de detecci�n, cambiar a la siguiente fase
        if (Vector3.Distance(transform.position, player.position) < detectionRange)
        {
            isPlayerDetected = true;
        }
    }

    //Sigue al jugador y vuelve a la posicion
    void ChasePlayer()
    {

        // Mover al enemigo hacia el jugador
        if (type == EnemyType.PatrullaMelee)
        {
            agent.SetDestination(player.position);

        }
        else if (type == EnemyType.PatrullaArcher)
        {
            MirarJugador();
            MantenerDist();
        }


        if (Vector3.Distance(transform.position, player.position) > detectionRange)
        {
            isPlayerDetected = false;
        }
    }

    //Sigue al jugador indefinidamente
    public void DetectarJugador()
    {
        //RaycastHit hit;
        //if (Physics.Raycast(transform.position, player.position - transform.position, out hit, detectionRange))
        //{
            //if (hit.collider.gameObject.CompareTag("Player"))
                if (Vector3.Distance(transform.position, player.position) < detectionRange)
                {
                    if (type== EnemyType.SpawnerMelee)
                    {
                        agent.SetDestination(player.position);
                    //if(transform.position=)
                    }
                else if (type == EnemyType.SpawnerArcher)
                {
                    MirarJugador();
                    MantenerDist();
                }
                else if(type == EnemyType.PlantaTorreta)
                {
                    MirarJugador();

                }
               
            }
        //}

    }
  

    private void MirarJugador()
    {
        //enemy.LookAt(player.position);
        timetoShoot -= Time.deltaTime;
        if (timetoShoot < 0)
        {
            ShootPlayer();
            timetoShoot = originalTime;
        }
    }
  private void ShootPlayer()
    {
        GameObject currentbullet = Instantiate(bala, zonadisparo.position, zonadisparo.rotation);
        Rigidbody rig = currentbullet.GetComponent<Rigidbody>();

        rig.AddForce(transform.forward * shootspeed, ForceMode.VelocityChange);
    }

    private void MantenerDist()
    {
        if (Vector3.Distance(transform.position, player.position) > rangodetenerse)
        {
            agent.destination = player.position;
           // transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
        else if (Vector3.Distance(transform.position, player.position) < rangodetenerse && Vector3.Distance(transform.position, player.position) > rangodistanciar)
        {
            agent.destination = this.transform.position;
            //transform.position = this.transform.position;
        }
        else if (Vector3.Distance(transform.position, player.position) < rangodistanciar)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
        } 
    }

    private void OnTriggerEnter(Collider other)
    {
       
        if (other.tag == "Melee")
        {
      
            float damagenormal = GameObject.Find("Player").GetComponent<Player>()._characterData.damageMelee;
           TakeDamage(damagenormal);
           
        }
        else if (other.CompareTag("Projectile"))
        {
            float damageespecial = GetComponent<Player>().damageSpecial;
            TakeDamage(damageespecial);
        }
       
    }

    public void TakeDamage(float damage)
    {  
        hp -= damage;
        if (hp  <= 0)
        {
            Muerte();
        }
        
        Debug.Log("Health: " + hp);
        
    }

    public void trapped()
    {
        
        agent.SetDestination(transform.position);
        Debug.Log("Cirulo");
        
        StartCoroutine(WaitTrapped());
    }

    IEnumerator WaitTrapped()
    {
        Debug.Log(agent.destination);
        var state = currentState;
        Debug.Log(state);
        currentState = EnemyType.Trapped;
        yield return new WaitForSeconds(5f);
        agent.SetDestination(player.position);

        currentState = state;
    }
    private void Muerte()
    {
        if (hp<=0)
        {
            //Animacion Muerte Enemie
            Destroy(gameObject, 2);
        }
    }

   
}
