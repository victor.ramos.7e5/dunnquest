using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateController : MonoBehaviour
{
    public ScriptableState currentState;

    void Update()
    {
        currentState.action.OnUpdate(this);   
    }
    public void StateTransition(ScriptableState state)
    {

       // Debug.Log(currentState.StateTransition.Contains(state));
        if (currentState.StateTransition.Contains(state))
        {
            currentState.action.OnFinishState();
            currentState = state;
            currentState.action.OnSetState(this);
        }
    }
}
