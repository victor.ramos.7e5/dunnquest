using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : StateController
{
    private float hp, range;
    public EnemiesSO _SOenemy;
    private float rangodistanciar;
    private float rangodetenerse;
    //float originalTime;


    public NavMeshAgent agent;

    public ScriptableState Patrol, Follow, Attack, Die,Trap;
    private Transform player;


    private bool isPlayerDetected = false;

    [SerializeField] private AudioSource audiosource;

    private void Start()
    {
        currentState.action.OnSetState(this);
        //originalTime = timetoShoot;
        player = GameObject.Find("Player").transform;
        hp = _SOenemy.hp;
        range = _SOenemy.range;
        rangodistanciar = _SOenemy.rangodistanciar;
        rangodetenerse = _SOenemy.rangodetenerse;
        agent = GetComponent<NavMeshAgent>();
    }


    void Update()
    {
       // Debug.Log(currentState);

        currentState.action.OnUpdate(this);
        DetectPlayer();
    }
    private void DetectPlayer()
    {
        if (!isPlayerDetected)
        {
            // Si el jugador no ha sido detectado, continuar patrullando entre los waypoints
            StateTransition(Patrol);
            //Debug.Log(player);
            try
            {
                if (Vector3.Distance(transform.position, player.position) < range)
                {
                    isPlayerDetected = true;
                }
            }
            catch { }
        }
        else
        {

            //StateTransition(Attack);

            if (Vector3.Distance(transform.position, player.position) < 2)
            {
                StateTransition(Attack);

            }
            else if (Vector3.Distance(transform.position, player.position) > range)
            {
                isPlayerDetected = false;
            }
            else
            {
                StateTransition(Follow);
            }
        }
    }
   


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Trap") // Poner tag de la tramp
        {
            Debug.Log("Trampa");
            Traped();
        }
        if (other.gameObject.CompareTag("Arma"))
        {
            float armadamage = other.GetComponent<Arma>().damage;
            TakeDamage(armadamage);
        }
        /*if (other.gameObject.CompareTag("Projectile"))
        {
            TakeDamage(15f);
        }*/
    }

    public void TakeDamage(float damage)
    {
        hp -= damage;
        Debug.Log("Health: " + hp);
        audiosource.Play();
        if (hp<=0)
        {
            Death();
        }
    }
    private void Death()
    {
        StateTransition(Die);
    }
    private void Traped()
    {
        StateTransition(Trap);
        StartCoroutine(WaitTrapped());
    }
    IEnumerator WaitTrapped()
    {
        yield return new WaitForSeconds(5f);
        StateTransition(Follow);

    }
}
