using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="ScriptableAttack",menuName = "ScriptableObjects/ScriptableAction/ScriptableAttack")]
public class ScriptableAttack : ScriptableAction
{

    public override void OnFinishState()
    {
        //_moveBehaviour.Perseguir();

        //_attackBehaviour.AttackMelee();

    }

    public override void OnSetState(StateController sc)
    {

        
        sc.GetComponent<AttackBehaviour>().EnFrenteAlJugador();
        sc.GetComponent<AttackBehaviour>().Atacar();
    }

    public override void OnUpdate(StateController sc)
    {
        var _attackBehaviour = sc.GetComponent<AttackBehaviour>();
        _attackBehaviour.EnFrenteAlJugador();
        _attackBehaviour.Atacar();

    }


}
