using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableDie", menuName = "ScriptableObjects/ScriptableAction/ScriptableDie")]

public class ScriptableDie : ScriptableAction
{
    private MoveBehaviour _muelte;


    public override void OnFinishState()
    {
        Debug.Log("MueltoFinis");
    }

    public override void OnSetState(StateController sc)
    {
        _muelte = sc.GetComponent<MoveBehaviour>();

    }

    public override void OnUpdate(StateController sc)
    {
        _muelte.Die();

    }


}
