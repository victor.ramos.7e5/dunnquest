using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(fileName = "ScriptablePatrol", menuName = "ScriptableObjects/ScriptableAction/ScriptablePatrol", order = 3)]

public class ScriptablePatrol : ScriptableAction
{


    public override void OnFinishState()
    {
        
    }

    public override void OnSetState(StateController sc)
    {

        sc.GetComponent<MoveBehaviour>().PatrolWaypoints();

    }

    public override void OnUpdate(StateController sc)
    {

        sc.GetComponent<MoveBehaviour>().PatrolWaypoints();


    }


}
