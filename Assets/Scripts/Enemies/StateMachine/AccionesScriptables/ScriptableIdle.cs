using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableIdle", menuName = "ScriptableObjects/ScriptableAction/ScriptableIdle")]

public class ScriptableIdle : ScriptableAction
{
    private MoveBehaviour _folowPlayer;
    public override void OnFinishState()
    {
       // _folowPlayer.Perseguir();
    }

    public override void OnSetState(StateController sc)
    {
        //_folowPlayer = sc.GetComponent<MoveBehaviour>();
    }

    public override void OnUpdate(StateController sc)
    {

        sc.GetComponent<MoveBehaviour>().StopChase();
    }

}
