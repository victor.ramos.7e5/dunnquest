using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableTrap", menuName = "ScriptableObjects/ScriptableAction/ScriptableTrap", order = 3)]

public class SciptableTrap : ScriptableAction
{
    private MoveBehaviour _moveBehaviour;

    private EnemyController _enemyController;
    public override void OnFinishState()
    {
        _moveBehaviour.Perseguir();
    }

    public override void OnSetState(StateController sc)
    {
        _moveBehaviour = sc.GetComponent<MoveBehaviour>();

    }

    public override void OnUpdate(StateController sc)
    {
        _moveBehaviour.trapped();
    }

   
}
