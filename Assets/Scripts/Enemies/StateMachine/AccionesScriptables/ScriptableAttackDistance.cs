using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableDistance", menuName = "ScriptableObjects/ScriptableAction/ScriptableDistance")]

public class ScriptableAttackDistance : ScriptableAction
{
    private AttackBehaviour _attackdistance;
   
    public override void OnFinishState()
    {
        //_attackdistance.ShootPlayer();

    }

    public override void OnSetState(StateController sc)
    {
        //_attackdistance = sc.GetComponent<AttackBehaviour>();
        sc.GetComponent<MoveBehaviour>().Perseguir();
    }

    public override void OnUpdate(StateController sc)
    {
        sc.GetComponent<AttackBehaviour>().MirarJugador();
        sc.GetComponent<AttackBehaviour>().MantenerDist();
    }

   
}
