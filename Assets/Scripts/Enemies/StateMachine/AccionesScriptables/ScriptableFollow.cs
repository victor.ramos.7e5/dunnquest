using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableFollow", menuName = "ScriptableObjects/ScriptableAction/ScriptableFollow")]

public class ScriptableFollow : ScriptableAction
{
    private MoveBehaviour _folowPlayer;

    public override void OnFinishState()
    {
        //_folowPlayer.PatrolWaypoints();
    }

    public override void OnSetState(StateController sc)
    {

        sc.GetComponent<MoveBehaviour>().Perseguir();


    }

    public override void OnUpdate(StateController sc)
    {
        sc.GetComponent<MoveBehaviour>().Perseguir();
    }

}
