using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="ScriptableState", menuName ="ScriptableObjects/ScriptableState",order =3)]
public class ScriptableState : ScriptableObject
{
    public ScriptableAction action;
    public List<ScriptableState> StateTransition;
}
