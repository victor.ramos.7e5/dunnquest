using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController2 : StateController
{
    private float hp, damage, speed, range, cooldown;
    public EnemiesSO _SOenemy;
    private float rangodistanciar;
    private float rangodetenerse;
    float originalTime;
    private float timetoShoot = 1.3f;
    public NavMeshAgent agent;
    private Arma arma;
    public ScriptableState idle;
    private Transform player;
    public ScriptableState Follow, Attack, Die,Trap;
    [SerializeField]
    private AudioSource audiosource;



    private bool isPlayerDetected = false;

    private void Start()
    {
        currentState.action.OnSetState(this);
        originalTime = timetoShoot;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        hp = _SOenemy.hp;
        range = _SOenemy.range;
        damage = _SOenemy.damage;
        speed = _SOenemy.speed;
        rangodistanciar = _SOenemy.rangodistanciar;
        rangodetenerse = _SOenemy.rangodetenerse;
        agent = GetComponent<NavMeshAgent>();
    }


    void Update()
    {
        currentState.action.OnUpdate(this);
        DetectPlayer();

    }
    private void DetectPlayer()
    {
        if (Vector3.Distance(transform.position, player.position) < range)
        {
            StateTransition(Follow);
            if (Vector3.Distance(transform.position, player.position) < range)
            {
                StateTransition(Attack);

            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag=="Player")
        {
            StateTransition(Attack);
        }
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Trap")) // Poner tag de la tramp
        {
            Debug.Log("Trampa");
            Traped();
        }
        if (other.gameObject.CompareTag("Arma"))
        {
            float armadamage = other.GetComponent<Arma>().damage;
            TakeDamage(armadamage);
        } /*if (other.gameObject.CompareTag("Projectile"))
        {
           
            TakeDamage(15f);
        }*/
    }

    public void TakeDamage(float damage)
    {
        hp -= damage;
        audiosource.Play();
        if (hp <= 0)
        {
            Death();
        }
    }
    private void Death()
    {
        StateTransition(Die);
    }
    private void Traped()
    {
        StateTransition(Trap);
        StartCoroutine(WaitTrapped());
    }
    IEnumerator WaitTrapped()
    {

        yield return new WaitForSeconds(5f);
        StateTransition(Follow);

    }
}
