using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class MoveBehaviour : MonoBehaviour
{
    public Transform[] waypoints;
    public NavMeshAgent agent;
    private Transform player;
    private int currentWaypointIndex = 0;
    private Animator anim;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
    }
    public void Perseguir()
    {
        agent.isStopped = false;
        anim.SetBool("Walk",true);
        agent.SetDestination(player.position);

    }
    public void StopChase()
    {
        agent.isStopped=true;
    }
   
    public void PatrolWaypoints()
    {
        agent.destination = waypoints[currentWaypointIndex].position;
        anim.SetBool("Walk", true);

        if (Vector3.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(waypoints[currentWaypointIndex].position.x, waypoints[currentWaypointIndex].position.z)) < 0.65f)
        {
            currentWaypointIndex = (currentWaypointIndex + 1) % waypoints.Length;
        }

        // Si el jugador est� dentro del rango de detecci�n, cambiar a la siguiente fase
        /*if (Vector3.Distance(transform.position, player.position) < detectionRange)
        {
            isPlayerDetected = true;
        }*/
    }

    public void Die()
    {
        StopChase();
        anim.SetBool("Die", true);
        Destroy(this.gameObject, 5);
    }
    public void trapped()
    {

        agent.SetDestination(transform.position);
        Debug.Log("Cirulo");

        StartCoroutine(WaitTrapped());
    }

    IEnumerator WaitTrapped()
    {
       
        yield return new WaitForSeconds(5f);
        agent.SetDestination(player.position);

        
    }
}


