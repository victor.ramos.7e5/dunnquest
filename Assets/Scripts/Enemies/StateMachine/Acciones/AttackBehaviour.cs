using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackBehaviour : MonoBehaviour
{
    public GameObject bala;
    public Transform zonadisparo;
    private float shootspeed = 10f;
    private float timetoShoot = 1.3f;

    private float rangodistanciar = 4;
    private float rangodetenerse = 4;

    private Transform player;
    public NavMeshAgent agent;


    public float speed = 3;
    float originalTime;
    private Animator anim;


    private bool atacando;
    private bool mirando;

    void Start()
    {
        originalTime = timetoShoot;

        player = GameObject.FindGameObjectWithTag("Player").transform;

        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();


    }
    public void MirarJugador()
    {
        //enemy.LookAt(player.position);
        timetoShoot -= Time.deltaTime;
        if (timetoShoot < 0)
        {
            ShootPlayer();
            timetoShoot = originalTime;
        }
    }

    public void EnFrenteAlJugador()
    {
        Vector3 adelante = transform.forward;
        Vector3 targetjug = (player.transform.position - transform.position).normalized;
        if (Vector3.Dot(adelante, targetjug) < 0.6)
        {
            mirando = false;
        }
        else
        {
            mirando = true;
        }
    }
    public void Atacar()
    {
        if (Vector3.Distance(player.transform.position, transform.position) <= 2 && mirando)
        {
            Ataque();

        }
        //agent.SetDestination(player.position);

    }
    void Ataque()
    {
        atacando = true;
        anim.SetBool("Melee",true);
        Invoke("ReiniciarAtaque", 0.5f);
    }
    void ReiniciarAtaque()
    {
        anim.SetBool("Melee", false);

        atacando = false;
    }
    public void ShootPlayer()
    {
        GameObject currentbullet = Instantiate(bala, zonadisparo.position, zonadisparo.rotation);
        Rigidbody rig = currentbullet.GetComponent<Rigidbody>();
        anim.SetTrigger("Archer");

        rig.AddForce(transform.forward * shootspeed, ForceMode.VelocityChange);
    }

    public void MantenerDist()
    {
        if (Vector3.Distance(transform.position, player.position) > rangodetenerse)
        {
            agent.destination = player.position;
            // transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
        else if (Vector3.Distance(transform.position, player.position) < rangodetenerse && Vector3.Distance(transform.position, player.position) > rangodistanciar)
        {
            agent.destination = this.transform.position;
            //transform.position = this.transform.position;
        }
        else if (Vector3.Distance(transform.position, player.position) < rangodistanciar)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
        }
    }


}
