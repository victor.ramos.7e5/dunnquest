using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
   public float tiempoDestruccion = 3f;
    // Start is called before the first frame update
    void Start()
    {
       Destroy(gameObject, tiempoDestruccion);
    }

    void OnTriggerEnter(Collider other)
    {
        // Si la bala colisiona con el jugador, le hace da�o
        if (other.CompareTag("Player"))
        {
            //Hacer da�o player
            Destroy(gameObject);
        }

    }
}
