using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitiesCooldown : MonoBehaviour
{
    public Scout scout;
    public Slider Skill1;
    public Slider Skill2;
    public Slider Skill3;
    public Slider Skill4;

    void Start()
    {
        
    }


    void Update()
    {
        Skill1.value -= Time.deltaTime/scout.CoolDownAbility1;
        Skill2.value -= Time.deltaTime/scout.CoolDownAbility2;
        Skill3.value -= Time.deltaTime/scout.CoolDownAbility3;
        Skill4.value -= Time.deltaTime/scout.CoolDownAbility4;
    }
}
